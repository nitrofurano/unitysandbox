﻿using System.Collections;using System.Collections.Generic;using UnityEngine;
public class CameraFollowRotate:MonoBehaviour{
  [SerializeField] private Transform target;
  [SerializeField] private Vector3 offsetPosition;
  [SerializeField] private Space offsetPositionSpace=Space.Self;
  [SerializeField] private bool lookAt=true;
  private void LateUpdate(){
    Refresh();}
  public void Refresh(){
    if (target==null){
      Debug.LogWarning("Missing target ref !",this);
      return;}
    if (offsetPositionSpace==Space.Self){
      transform.position=target.TransformPoint(offsetPosition);}
    else{
      transform.position=target.position+offsetPosition;}
    if (lookAt){
      transform.LookAt(target);transform.Rotate(-15,0,0);}
    else{
      transform.rotation=target.rotation;}}}
