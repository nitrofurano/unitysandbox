﻿using System.Collections;using System.Collections.Generic;using UnityEngine;
public class MovePlayer:MonoBehaviour{
  public float speed=5.0f;
  public float jumpSpeed=15.0f;
  public float gravity=15.0f;
  public float rotateSpeed=10.0f;
  private Vector3 moveDirection=Vector3.zero;
  void Start(){}
  void Update(){
    if (Input.GetKeyDown(KeyCode.Escape)){Application.Quit();}
    CharacterController controller=GetComponent<CharacterController>();
    if (transform.position.y<-10){
      moveDirection=new Vector3(0,0,0);
      transform.position=new Vector3(0,50,0);}
    if (controller.isGrounded){
      moveDirection=new Vector3(0,0,Input.GetAxis("Vertical"));
      moveDirection=transform.TransformDirection(moveDirection);
      moveDirection*=speed;
      if (Input.GetButton("Jump")){
        moveDirection.y=jumpSpeed;}}
    moveDirection.y-=gravity*Time.deltaTime;
    controller.Move(moveDirection*Time.deltaTime);
    transform.Rotate(0,Input.GetAxis("Horizontal")*rotateSpeed,0);}}

