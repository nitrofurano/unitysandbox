﻿using System.Collections;using System.Collections.Generic;using UnityEngine;using UnityEngine.UI;
public class main : MonoBehaviour{

  public float range,ihorizm,ivertm;
  public Text textOutput1,textOutput2,textOutput3,textOutput4;
  public int hgarbage1,vgarbage1,ubaxis,xyspeed,counter,counter2;
  public int keybd,xpos,ypos,xoff,yoff;
  public int iforce,igravity,ipressed,igrlim;
  public GameObject sprite1;

  void Start(){
    counter=0;xyspeed=8;
    xpos=450;ypos=250;xoff=-360;yoff=-225;
    igrlim=20;iforce=igrlim*-1;igravity=iforce;ipressed=0;
    }

  void Update (){

    counter2=(counter/50)%1000;

    if (xpos<-0){xpos=720;}
    if (xpos>720){xpos=0;}
    if (ypos<0){ypos=450;}
    if (ypos>450){ypos=0;}

    keybd=0;
    if (Input.GetKey(KeyCode.LeftArrow)){keybd+=1;xpos-=xyspeed;}
    if (Input.GetKey(KeyCode.RightArrow)){keybd+=2;xpos+=xyspeed;}

    ypos+=(igravity/1);
    igravity+=1;

    if (igravity>igrlim){igravity=igrlim;}
    if (Input.GetKey(KeyCode.Z) && ipressed==0){igravity=iforce;ipressed=1;keybd+=16;}
    if ( !Input.GetKey(KeyCode.Z) ){ipressed=0;}

    textOutput1.text=igravity.ToString("F0");
    textOutput2.text=keybd.ToString("F0");
    textOutput3.text=xpos.ToString("F0")+","+ypos.ToString("F0");
    textOutput4.text=counter2.ToString("F0");
    sprite1.transform.position = new Vector2(xpos+xoff,(ypos+yoff)*-1);

    counter+=1;

    }}


