#!/bin/bash
rm -r ./Library/*
rmdir ./Library
rm -r ./Logs/*
rmdir ./Logs
rm -r ./Packages/*
rmdir ./Packages
rm -r ./Temp/*
rmdir ./Temp
#!/bin/bash
rm UnityPlayer*
rm LinuxPlayer*
rm *.x86_64
rm -r ./*_Data/*
rmdir ./*_Data

