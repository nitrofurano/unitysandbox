﻿using System.Collections;using System.Collections.Generic;using UnityEngine;using UnityEngine.UI;
public class main : MonoBehaviour{

  public float range,ihorizm,ivertm;
  public Text textOutput0,textOutput1,textOutput2,textOutput3;
  public Text textOutputB1,textOutputB2;
  public int hgarbage1,vgarbage1,ubaxis;
  public int counter,counter2;
  public int trailcounter,arrpos,arrdist;
  public int keybd,keybd0,keybd1,keybd2,keybd3;
  public int isjumpn0,isjumpn1,isjumpn2,isjumpn3,ismovn;
  public float xyspeed;
  public float xpos0,ypos0,xyspeed0,zpos0,zgra0;
  public float xpos1,ypos1,xyspeed1,zpos1,zgra1;
  public float xpos2,ypos2,xyspeed2,zpos2,zgra2;
  public float xpos3,ypos3,xyspeed3,zpos3,zgra3;
  public float grainc;
  public GameObject sprite0,sprite1,sprite2,sprite3;

  public float[] xarr = new float[512];
  public float[] yarr = new float[512];
  public float[] zarr = new float[512];

  void Start(){
    counter=0;xyspeed=.1f;grainc=.1f;
    xpos0=0f;ypos0=3f;zpos0=.5f;zgra0=0f;isjumpn0=0;
    xpos1=3f;ypos1=0f;zpos1=.5f;zgra1=0f;isjumpn1=0;
    xpos2=0f;ypos2=-3f;zpos2=.5f;zgra2=0f;isjumpn2=0;
    xpos3=-3f;ypos3=0f;zpos3=.5f;zgra3=0f;isjumpn3=0;
    arrpos=0;arrdist=16;
    }

  void FixedUpdate (){
    ismovn=0;
    grainc=.1f;

    keybd0=0;
    if (Input.GetKey(KeyCode.UpArrow)){keybd0+=4;ypos0-=xyspeed;ismovn=1;}
    if (Input.GetKey(KeyCode.DownArrow)){keybd0+=8;ypos0+=xyspeed;ismovn=1;}
    if (Input.GetKey(KeyCode.LeftArrow)){keybd0+=1;xpos0-=xyspeed;ismovn=1;}
    if (Input.GetKey(KeyCode.RightArrow)){keybd0+=2;xpos0+=xyspeed;ismovn=1;}
    if (Input.GetKey(KeyCode.Z) && isjumpn0==0){keybd0+=16;zgra0=-.7f;isjumpn0=1;}
 
    xpos1=xarr[(trailcounter+(256-(arrdist*1)))%256];ypos1=yarr[(trailcounter+(256-(arrdist*1)))%256];zpos1=zarr[(trailcounter+(256-(arrdist*1)))%256];
    xpos2=xarr[(trailcounter+(256-(arrdist*2)))%256];ypos2=yarr[(trailcounter+(256-(arrdist*2)))%256];zpos2=zarr[(trailcounter+(256-(arrdist*2)))%256];
    xpos3=xarr[(trailcounter+(256-(arrdist*3)))%256];ypos3=yarr[(trailcounter+(256-(arrdist*3)))%256];zpos3=zarr[(trailcounter+(256-(arrdist*3)))%256];

    if (Input.GetKey(KeyCode.Escape)){Application.Quit();}

    if (xpos0>4.5f){xpos0=-4.5f;}
    if (xpos0<-4.5f){xpos0=4.5f;}
    if (ypos0>4.5f){ypos0=-4.5f;}
    if (ypos0<-4.5f){ypos0=4.5f;}

    if (isjumpn0!=0){zpos0+=zgra0;zgra0+=grainc;ismovn=1;}
    if (zpos0>-.5f){zpos0=-.5f;zgra0=0f;grainc=0f;isjumpn0=0;}

    if (ismovn!=0){
      xarr[trailcounter%256]=xpos0;
      yarr[trailcounter%256]=ypos0;
      zarr[trailcounter%256]=zpos0;
      ismovn=0;
      trailcounter+=1;
      }

    counter2=(counter/50)%1000;

    textOutput0.text=xpos0.ToString("F1")+","+ypos0.ToString("F1")+","+zpos0.ToString("F1")+","+zgra0.ToString("F1")+","+keybd0.ToString("F0");
    textOutput1.text=xpos1.ToString("F1")+","+ypos1.ToString("F1")+","+zpos1.ToString("F1")+","+zgra1.ToString("F1")+","+keybd1.ToString("F0");
    textOutput2.text=xpos2.ToString("F1")+","+ypos2.ToString("F1")+","+zpos2.ToString("F1")+","+zgra2.ToString("F1")+","+keybd2.ToString("F0");
    textOutput3.text=xpos3.ToString("F1")+","+ypos3.ToString("F1")+","+zpos3.ToString("F1")+","+zgra3.ToString("F1")+","+keybd3.ToString("F0");
    textOutputB1.text=counter2.ToString("F0");
    textOutputB2.text=(trailcounter%256).ToString("F0");

    sprite0.transform.position = new Vector3(xpos0,zpos0*-1,ypos0*-1);
    sprite1.transform.position = new Vector3(xpos1,zpos1*-1,ypos1*-1);
    sprite2.transform.position = new Vector3(xpos2,zpos2*-1,ypos2*-1);
    sprite3.transform.position = new Vector3(xpos3,zpos3*-1,ypos3*-1);

    counter+=1;
    }}


