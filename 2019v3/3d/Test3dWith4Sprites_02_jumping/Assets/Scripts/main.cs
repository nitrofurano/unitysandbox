﻿using System.Collections;using System.Collections.Generic;using UnityEngine;using UnityEngine.UI;
public class main : MonoBehaviour{

  public float range,ihorizm,ivertm;
  public Text textOutput0,textOutput1,textOutput2,textOutput3;
  public Text textOutputB1,textOutputB2;
  public int hgarbage1,vgarbage1,ubaxis,counter,counter2;
  public int keybd,keybd0,keybd1,keybd2,keybd3;
  public int isjumpn0,isjumpn1,isjumpn2,isjumpn3;
  public float xyspeed;
  public float xpos0,ypos0,xyspeed0,zpos0,zgra0;
  public float xpos1,ypos1,xyspeed1,zpos1,zgra1;
  public float xpos2,ypos2,xyspeed2,zpos2,zgra2;
  public float xpos3,ypos3,xyspeed3,zpos3,zgra3;
  public float grainc;
  public GameObject sprite0,sprite1,sprite2,sprite3;

  void Start(){
    counter=0;xyspeed=.1f;grainc=.1f;
    xpos0=0f;ypos0=3f;zpos0=.5f;zgra0=0f;isjumpn0=0;
    xpos1=3f;ypos1=0f;zpos1=.5f;zgra1=0f;isjumpn1=0;
    xpos2=0f;ypos2=-3f;zpos2=.5f;zgra2=0f;isjumpn2=0;
    xpos3=-3f;ypos3=0f;zpos3=.5f;zgra3=0f;isjumpn3=0;
    }

  void FixedUpdate (){

    grainc=.1f;

    keybd0=0;
    if (Input.GetKey(KeyCode.UpArrow)){keybd0+=4;ypos0-=xyspeed;}
    if (Input.GetKey(KeyCode.DownArrow)){keybd0+=8;ypos0+=xyspeed;}
    if (Input.GetKey(KeyCode.LeftArrow)){keybd0+=1;xpos0-=xyspeed;}
    if (Input.GetKey(KeyCode.RightArrow)){keybd0+=2;xpos0+=xyspeed;}
    //if (Input.GetKey(KeyCode.Keypad0) && isjumpn0==0){keybd0+=16;zgra0=-.7f;isjumpn0=1;}
    if (Input.GetKey(KeyCode.RightControl) && isjumpn0==0){keybd0+=16;zgra0=-.7f;isjumpn0=1;}
    keybd1=0;
    if (Input.GetKey(KeyCode.W)){keybd1+=4;ypos1-=xyspeed;}
    if (Input.GetKey(KeyCode.S)){keybd1+=8;ypos1+=xyspeed;}
    if (Input.GetKey(KeyCode.A)){keybd1+=1;xpos1-=xyspeed;}
    if (Input.GetKey(KeyCode.D)){keybd1+=2;xpos1+=xyspeed;}
    if (Input.GetKey(KeyCode.Q) && isjumpn1==0){keybd1+=16;zgra1=-.7f;isjumpn1=1;}
    keybd2=0;
    if (Input.GetKey(KeyCode.T)){keybd2+=4;ypos2-=xyspeed;}
    if (Input.GetKey(KeyCode.G)){keybd2+=8;ypos2+=xyspeed;}
    if (Input.GetKey(KeyCode.F)){keybd2+=1;xpos2-=xyspeed;}
    if (Input.GetKey(KeyCode.H)){keybd2+=2;xpos2+=xyspeed;}
    if (Input.GetKey(KeyCode.R) && isjumpn2==0){keybd2+=16;zgra2=-.7f;isjumpn2=1;}
    keybd3=0;
    if (Input.GetKey(KeyCode.I)){keybd3+=4;ypos3-=xyspeed;}
    if (Input.GetKey(KeyCode.K)){keybd3+=8;ypos3+=xyspeed;}
    if (Input.GetKey(KeyCode.J)){keybd3+=1;xpos3-=xyspeed;}
    if (Input.GetKey(KeyCode.L)){keybd3+=2;xpos3+=xyspeed;}
    if (Input.GetKey(KeyCode.U) && isjumpn3==0){keybd3+=16;zgra3=-.7f;isjumpn3=1;}

    if (Input.GetKey(KeyCode.Escape)){Application.Quit();}

    if (xpos0>4.5f){xpos0=-4.5f;}
    if (xpos0<-4.5f){xpos0=4.5f;}
    if (ypos0>4.5f){ypos0=-4.5f;}
    if (ypos0<-4.5f){ypos0=4.5f;}
    if (xpos1>4.5f){xpos1=-4.5f;}
    if (xpos1<-4.5f){xpos1=4.5f;}
    if (ypos1>4.5f){ypos1=-4.5f;}
    if (ypos1<-4.5f){ypos1=4.5f;}
    if (xpos2>4.5f){xpos2=-4.5f;}
    if (xpos2<-4.5f){xpos2=4.5f;}
    if (ypos2>4.5f){ypos2=-4.5f;}
    if (ypos2<-4.5f){ypos2=4.5f;}
    if (xpos3>4.5f){xpos3=-4.5f;}
    if (xpos3<-4.5f){xpos3=4.5f;}
    if (ypos3>4.5f){ypos3=-4.5f;}
    if (ypos3<-4.5f){ypos3=4.5f;}

    if (isjumpn0!=0){zpos0+=zgra0;zgra0+=grainc;}
    if (zpos0>-.5f){zpos0=-.5f;zgra0=0f;grainc=0f;isjumpn0=0;}
    if (isjumpn1!=0){zpos1+=zgra1;zgra1+=grainc;}
    if (zpos1>-.5f){zpos1=-.5f;zgra1=0f;grainc=0f;isjumpn1=0;}
    if (isjumpn2!=0){zpos2+=zgra2;zgra2+=grainc;}
    if (zpos2>-.5f){zpos2=-.5f;zgra2=0f;grainc=0f;isjumpn2=0;}
    if (isjumpn3!=0){zpos3+=zgra3;zgra3+=grainc;}
    if (zpos3>-.5f){zpos3=-.5f;zgra3=0f;grainc=0f;isjumpn3=0;}

    counter2=(counter/50)%1000;

    textOutput0.text=xpos0.ToString("F1")+","+ypos0.ToString("F1")+","+zpos0.ToString("F1")+","+zgra0.ToString("F1")+","+keybd0.ToString("F0");
    textOutput1.text=xpos1.ToString("F1")+","+ypos1.ToString("F1")+","+zpos1.ToString("F1")+","+zgra1.ToString("F1")+","+keybd1.ToString("F0");
    textOutput2.text=xpos2.ToString("F1")+","+ypos2.ToString("F1")+","+zpos2.ToString("F1")+","+zgra2.ToString("F1")+","+keybd2.ToString("F0");
    textOutput3.text=xpos3.ToString("F1")+","+ypos3.ToString("F1")+","+zpos3.ToString("F1")+","+zgra3.ToString("F1")+","+keybd3.ToString("F0");
    textOutputB1.text=counter2.ToString("F0");
    textOutputB2.text="";

    sprite0.transform.position = new Vector3(xpos0,zpos0*-1,ypos0*-1);
    sprite1.transform.position = new Vector3(xpos1,zpos1*-1,ypos1*-1);
    sprite2.transform.position = new Vector3(xpos2,zpos2*-1,ypos2*-1);
    sprite3.transform.position = new Vector3(xpos3,zpos3*-1,ypos3*-1);

    counter+=1;
    }}


