﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
 
public class UI_Event_Receiver:MonoBehaviour{ 
  public void OnButtonClick(){
    var buttonpressed=EventSystem.current.currentSelectedGameObject;
    if (buttonpressed!=null)
      Debug.Log("Clicked on: "+buttonpressed.name);
      if (buttonpressed.name=="ButtonScene2to1")SceneManager.LoadScene("Scene1");
      if (buttonpressed.name=="ButtonScene3to1")SceneManager.LoadScene("Scene1");
      if (buttonpressed.name=="ButtonScene1to2")SceneManager.LoadScene("Scene2");
      if (buttonpressed.name=="ButtonScene3to2")SceneManager.LoadScene("Scene2");
      if (buttonpressed.name=="ButtonScene1to3")SceneManager.LoadScene("Scene3");
      if (buttonpressed.name=="ButtonScene2to3")SceneManager.LoadScene("Scene3");
    else
      Debug.Log("currentSelectedGameObject is null");
      }}
